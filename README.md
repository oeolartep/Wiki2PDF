# Wiki2PDF

[![pipeline status](https://gitlab.com/Andrea.Ligios/Wiki2PDF/badges/master/pipeline.svg)](https://gitlab.com/Andrea.Ligios/Wiki2PDF/commits/master)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)


> Turns Gitlab's Wiki pages into PDF

## Why

Gitlab is a very cool platform, it provides almost everything you need, 
but it lacks integration between the **Project Repository**, the **Wiki Repository** 
(yeah, Wiki resides in a separate repository), and the **/uploads** folder 
where files are automatically stored by the Wiki's `Attach a file` functionality.

While there's a lot of people asking about *writing TO Wiki from a Gitlab-CI pipeline*, 
I guess there's also a lot of people (like I was) hoping to be able to **export Wiki pages as PDF**.

It can be very useful, especially when there's no internet connection, 
or when the repository is self-hosted and we want to read our Wiki pages 
from outside our private network.

## How to Use It

Simply copy the content of this `.gitlab-ci.yml` into your `.gitlab-ci.yml`, 
and the pipeline will convert all your Wiki pages to PDF.

Pay attention to the spaces before the commands, **YAML is space-sensitive**, it will break on a bad indentation.

### How To Use It on a self-hosted Gitlab

Like above, but replaces the two occurrences of `gitlab.com` with your self-hosted domain name.

## How it works

1. It clones the Wiki repository, 
2. parses the Wiki pages for attachments, 
3. recreates the folder structure and download the referenced files,
4. creates a new **/wikis** folder so that it stands out as an untracked folder (and gets downloaded), and move the files there.

I've created a sample Wiki page which you can see transformed in PDF in the downloadable artifacts.

To try it yourself, simply fork the repository, create a sample Wiki page (mine won't get forked, since Wiki resides in a separate repository) 
and trigger the pipeline to see it getting transformed into PDF by GitLab-CI.

In the downloadable artifacts, along with the PDFs, you'll find the original Wiki pages and the **/uploads** folder containing the referenced resources, 
because if something goes wrong for a .md page, you'll be able to try to convert it manually with other tools (eg. Atom).

I've also left the two files used to create the resource folders and download the files, for debugging purposes.

## Known Issues

1. It breaks if no Wiki page exists. Simply create a Wiki page before applying it to your project.
2. Prawn is able to handle most of the images out there, but I got problems with **interlaced PNG**s. 
   The solution is to open them with GIMP (or whatever), resave them with the `Interlacing (Adam7)` option **unchecked**, then reupload them to the Wiki page.

## Disclaimer

I'm not an expert on all the technologies involved; 
I just had a need, and I filled it with this solution because, 
AFAIK, there are no out-of-the-box ways to do it.

It's surely perfectible, but it works, and it's better than nothing.

Any feedback is appreciated. Cheers.